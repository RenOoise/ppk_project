from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, SelectField
from wtforms.validators import ValidationError, DataRequired, Length
from app.models import User, Place, Line, TechnicalMaintenance, Employee, Equipment, Tasks, PromptMaintenance, Machine


class AddMaintenance(FlaskForm):
    description = StringField('Описание задания', validators=[DataRequired()])
    type = SelectField('Тип обслуживания', validators=[DataRequired()], choices=[('daily', 'Ежедневное'),
                                                                                 ('weekly', 'Еженедельное'),
                                                                                 ('monthly', 'Ежемесячное'),
                                                                                 ('half_early', 'Полугодичное'),
                                                                                 ('currently', 'Текущее'),
                                                                                 ('inspection', 'Послеосмотровое')])
    equipment_type = SelectField('Тип оборудования', choices=[], coerce=int)
    submit = SubmitField('Добавить')


class AddTypeOfEquipment(FlaskForm):
    name = StringField('Тип', validators=[DataRequired()])
    submit = SubmitField('Добавить')
