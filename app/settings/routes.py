from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_required
from app import db
from app.main.forms import EditProfileForm
from app.settings.forms import AddMaintenance, AddTypeOfEquipment
from app.models import User, Equipment, TechnicalMaintenanceList, Employee, Machine
from app.settings import bp


@bp.route('/settings', methods=['POST', 'GET'])
@login_required
def settings():

    return render_template('settings/settings.html', title='Настройки', settings=True, settings_active=True)


@bp.route('/settings/users', methods=['POST', 'GET'])
@login_required
def users():
    users_list = User.query.all()
    return render_template('settings/users.html', title='Пользователи', users=True, settings_active=True, users_list=users_list)


@bp.route('/settings/gallery', methods=['POST', 'GET'])
@login_required
def gallery():

    return render_template('settings/gallery.html', title='Галерея', settings_active=True, galery=True)


@bp.route('/settings/files', methods=['POST', 'GET'])
@login_required
def files():

    return render_template('settings/files.html', title='Файлы', settings_active=True, files=True)


@bp.route('/settings/places', methods=['POST', 'GET'])
@login_required
def places():

    return render_template('settings/places.html', title='Цеха', settings_active=True, places=True)


@bp.route('/settings/lines', methods=['POST', 'GET'])
@login_required
def lines():

    return render_template('settings/lines.html', title='Линии', settings_active=True, lines=True)


@bp.route('/settings/equipment', methods=['POST', 'GET'])
@login_required
def equipment():
    form = AddTypeOfEquipment()
    if form.validate_on_submit():
        types = Machine(name=form.name.data, user_id=current_user.id)
        db.session.add(types)
        db.session.commit()
        return redirect(url_for('settings.equipment'))
    exist_types = Machine.query.all()
    equipment = Equipment.query.filter(Equipment.user_id == current_user.id)

    return render_template('settings/equipment.html', title='Оборудование', equipment=True, settings_active=True, types=exist_types,
                           equipment_list=equipment, form=form)


@bp.route('/settings/employees', methods=['POST', 'GET'])
@login_required
def employees():
    employees_list = Employee.query.all()
    return render_template('settings/employees.html', title='Сотрудники', employees=True, settings_active=True, employees_list=employees_list)


@bp.route('/settings/maintenance', methods=['POST', 'GET'])
@login_required
def maintenance():
    categories = [(c.id, c.name) for c in Machine.query.all()]
    form = AddMaintenance(request.form)
    form.equipment_type.choices = categories

    maintenance_list = TechnicalMaintenanceList.query.all()
    if form.validate_on_submit():
        maintenance = TechnicalMaintenanceList(type_of_maintenance=form.type.data, description=form.description.data,
                                               equipment_type_id=form.equipment_type.data, user_id=current_user.id)
        db.session.add(maintenance)
        db.session.commit()
        flash('Успех!')
        return redirect(url_for('settings.maintenance'))

    return render_template('settings/maintenance.html', title='ППР', form=form, maintenance=True, settings_active=True,
                           maintenance_list=maintenance_list)


@bp.route('/settings/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.nickname = form.nickname.data
        current_user.first_name = form.first_name.data
        current_user.last_name = form.last_name.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Ваш профиль обновлен!')
        return redirect(url_for('main.user', username=current_user.username))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.nickname.data = current_user.nickname
        form.first_name.data = current_user.first_name
        form.last_name.data = current_user.last_name
        form.about_me.data = current_user.about_me
    return render_template('settings/edit_profile.html', title='Редактирование профиля', edit_profile=True,
                           settings_active=True, form=form)
