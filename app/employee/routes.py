from flask import render_template, flash, redirect, url_for, request, g
from flask_login import current_user, login_required
from app import db
from app.employee.forms import AddEmployee, EditEmployee
from app.models import Employee
from app.employee import bp
from app import photos


@bp.route('/employees')
@login_required
def employees():
    technicians = Employee.query.all()
    return render_template('employee/employees.html', title='Сотрудники', employees=True, technicians=technicians)


@bp.route('/employees/add_employee', methods=['POST', 'GET'])
@login_required
def add_employee():
    add_form = AddEmployee()
    if add_form.validate_on_submit():
        query = Employee(first_name=add_form.first_name.data, last_name=add_form.last_name.data,
                         patronymic_name=add_form.patronymic_name.data, position=add_form.position.data,
                         birth_date=add_form.birth_date.data, photo=add_form.photo.data,
                         phone_number=add_form.phone_number.data)
        db.session.add(query)
        db.session.commit()
        return redirect(url_for('employee.employees'))
    return render_template('employee/add_employee.html', title='Создание профиля сотрудника', add_employee=True,
                           add_form=add_form)


@bp.route('/employees/id<int:id>', methods=['GET', 'POST'])
@login_required
def employee_profile(id):
    profile_data = Employee.query.filter(Employee.id == id).first_or_404()
    edit_form = EditEmployee()
    if edit_form.validate_on_submit():
        query = Employee.query.filter(Employee.id == id).first_or_404()
        query.first_name = edit_form.first_name.data
        query.last_name = edit_form.last_name.data
        query.patronymic_name = edit_form.patronymic_name.data
        query.birth_date = edit_form.birth_date.data
        query.phone_number = edit_form.phone_number.data
        query.position = edit_form.position.data

        if edit_form.photo.data:
            filename = 'uploads/' + photos.save(edit_form.photo.data)
            query.photo = filename

        db.session.commit()
        flash('Ваши изменения сохранены')
        return redirect(url_for('employee.employee_profile', id=id))

    elif request.method == 'GET':
        query = Employee.query.filter(Employee.id == id).first_or_404()
        edit_form.first_name.data = query.first_name
        edit_form.last_name.data = query.last_name
        edit_form.patronymic_name.data = query.patronymic_name
        edit_form.phone_number.data = query.phone_number
        edit_form.birth_date.data = query.birth_date
        edit_form.position.data = query.position
    return render_template('employee/employee_profile.html', title=profile_data.last_name + ' ' +
                                                                   profile_data.first_name[:1] + '. ' +
                                                                   profile_data.patronymic_name[:1] + '.',
                           profile_data=profile_data, employees=True, edit_form=edit_form)
