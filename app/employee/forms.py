from flask_wtf import FlaskForm
from flask_wtf.file import FileRequired, FileField
from wtforms import StringField, SubmitField, DateField
from wtforms.validators import DataRequired


class AddEmployee(FlaskForm):
    last_name = StringField('Фамилия', validators=[DataRequired()])
    first_name = StringField('Имя', validators=[DataRequired()])
    patronymic_name = StringField('Отчество')
    birth_date = DateField('Дата рождения', format='%d-%m-%Y')
    position = StringField('Должность', validators=[DataRequired()])
    photo = FileField('Фотография')
    phone_number = StringField('Номер телефона', validators=[DataRequired()])
    submit = SubmitField('Добавить')


class EditEmployee(FlaskForm):
    last_name = StringField('Фамилия', validators=[DataRequired()])
    first_name = StringField('Имя', validators=[DataRequired()])
    patronymic_name = StringField('Отчество')
    birth_date = DateField('Дата рождения', format='%d-%m-%Y')
    position = StringField('Должность', validators=[DataRequired()])
    photo = FileField('Фотография')
    phone_number = StringField('Номер телефона', validators=[DataRequired()])
    submit = SubmitField('Изменить')
