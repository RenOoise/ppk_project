from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, SelectField, DateTimeField, DateField
from wtforms.validators import ValidationError, DataRequired, Length
from app.models import User, Place, Line


class EditProfileForm(FlaskForm):
    username = StringField('Логин', validators=[DataRequired()])
    nickname = StringField('Никнейм', validators=[DataRequired()])
    first_name = StringField('Имя', validators=[DataRequired()])
    last_name = StringField('Фамилия', validators=[DataRequired()])
    about_me = TextAreaField('Обо мне',
                             validators=[Length(min=0, max=140)])
    submit = SubmitField('Обновить профиль')

    def __init__(self, original_username, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username

    def validate_username(self, username):
        if username.data != self.original_username:
            user = User.query.filter_by(username=self.username.data).first()
            if user is not None:
                raise ValidationError('Пожалуйста, введите другое имя.')


class PostForm(FlaskForm):
    post = TextAreaField('Скажите что-нибудь', validators=[DataRequired()])
    submit = SubmitField('Отправить')


class AddPlace(FlaskForm):
    name = StringField('Наименование места', validators=[DataRequired()])
    description = TextAreaField('Краткое описание места', validators=[DataRequired()])
    submit = SubmitField('Добавить место')


class EditPlace(FlaskForm):
    name = StringField('Наименование места', validators=[DataRequired()])
    description = TextAreaField('Краткое описание места', validators=[DataRequired()])
    submit = SubmitField('Применить изменения')


class AddLine(FlaskForm):
    name = StringField('Наименование линии', validators=[DataRequired()])
    employee = SelectField('Ответственное лицо', choices=[], coerce=int)
    submit = SubmitField('Применить')


class AddEquipment(FlaskForm):
    name = StringField('Наименование оборудования', validators=[DataRequired()])
    rnkb = StringField('Номер RNKB')
    description = TextAreaField('Краткое описание оборудования')
    equipment_type = SelectField('Тип оборудования', choices=[], coerce=int)
    tech_data = TextAreaField('Технические данные')
    submit = SubmitField('Добавить оборудование')


class MessageForm(FlaskForm):
    message = TextAreaField('Сообщение', validators=[
        DataRequired(), Length(min=0, max=140)])
    submit = SubmitField('Отправить')


class CreateTask(FlaskForm):
    date = DateField('Дата',  format='%d.%m.%Y')
    place = SelectField('Цех', choices=[], coerce=int)
    line = SelectField('Линия', choices=[], coerce=int)
    equipment = SelectField('Оборудование', choices=[], coerce=int)
    description = TextAreaField('Описание задания')
    employee = SelectField('Исполнитель', choices=[], coerce=int)
    deadline_date = DateTimeField('Срок сдачи')
