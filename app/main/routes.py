from datetime import datetime
from flask import render_template, flash, redirect, url_for, request, g, jsonify
from flask_login import current_user, login_required
from flask_babel import get_locale
from app import db
from app.main.forms import AddEquipment, AddLine, AddPlace, EditPlace, MessageForm, CreateTask
from app.models import User, Place, Line, Equipment, Message, Notification, Machine, Employee, Tasks
from app.main import bp
from app import photos
from app import current_app


@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
    g.locale = str(get_locale())


''' РОУТ ГЛАВНОЙ СТРАНИЦЫ '''


@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
@login_required
def index():

    place = Place.query.all()
    return render_template('index.html', title='Главная', index=True, place=place, add_place=add_place,
                           user_id=current_user.id)


''' ОСНОВНЫЕ РОУТЫ ПРИЛОЖЕНИЯ, НЕ ЛЕЖАТЕЛЬНО ИХ ИЗМЕНЯТЬ '''


@bp.route('/messages')
@login_required
def messages():
    current_user.last_message_read_time = datetime.utcnow()
    current_user.last_message_read_time = datetime.utcnow()
    current_user.add_notification('unread_message_count', 0)
    db.session.commit()
    page = request.args.get('page', 1, type=int)
    messages = current_user.messages_received.order_by(
        Message.timestamp.desc()).paginate(
            page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.messages', page=messages.next_num) \
        if messages.has_next else None
    prev_url = url_for('main.messages', page=messages.prev_num) \
        if messages.has_prev else None
    return render_template('messages.html', messages=messages.items,
                           next_url=next_url, prev_url=prev_url)


@bp.route('/send_message/<recipient>', methods=['GET', 'POST'])
@login_required
def send_message(recipient):
    user = User.query.filter_by(username=recipient).first_or_404()
    form = MessageForm()
    if form.validate_on_submit():
        msg = Message(author=current_user, recipient=user,
                      body=form.message.data)
        user.add_notification('unread_message_count', user.new_messages())
        db.session.add(msg)
        db.session.commit()

        flash('Ваше сообщение было отправлено.')
        return redirect(url_for('main.user', username=recipient))
    return render_template('send_message.html', title='Отправить сообщение',
                           form=form, recipient=recipient)


@bp.route('/notifications')
@login_required
def notifications():
    since = request.args.get('since', 0.0, type=float)
    notifications = current_user.notifications.filter(
        Notification.timestamp > since).order_by(Notification.timestamp.asc())
    return jsonify([{
        'name': n.name,
        'data': n.get_data(),
        'timestamp': n.timestamp
    } for n in notifications])


@bp.route('/export_posts')
@login_required
def export_posts():
    if current_user.get_task_in_progress('export_posts'):
        flash('An export task is currently in progress')
    else:
        current_user.launch_task('export_posts', 'Exporting posts...')
        db.session.commit()
    return redirect(url_for('main.user', username=current_user.username))


''' '''                                             ''' '''
''' Расширенные роуты, добавленые в скелет приложения '''
''' '''                                             ''' '''


@bp.route('/add_place', methods=['POST', 'GET'])
@login_required
def add_place():
    add_place = AddPlace()
    if add_place.validate_on_submit():
        name = add_place.name.data
        query = Place(name=name, user_id=current_user.id)
        db.session.add(query)
        db.session.commit()
        flash('Ваши изменения успешно сохранены', category='success')
        return redirect(url_for('main.index'))

    return render_template('_add_place.html', title='Добавление нового места', add_place=add_place)


@bp.route('/edit_place/<int:place_id>', methods=['POST', 'GET'])
@login_required
def edit_place(place_id):
    place = Place.query.filter_by(id=place_id).first_or_404()
    edit_place_form = EditPlace()
    if edit_place_form.validate_on_submit():
        place.name = edit_place_form.name.data
        place.description = edit_place_form.description.data
        db.session.commit()
        flash('Данные успешно изменены')
        return redirect(url_for('main.index'))
    elif request.method == 'GET':
        edit_place_form.name.data = place.name
        edit_place_form.description = place.description
    return render_template('edit_place.html', title='Редактирование цеха', edit_place_form=edit_place_form, place_id=place_id)


@bp.route('/delete_place/<int:place_id>', methods=['POST', 'GET'])
@login_required
def delete_place(place_id):
    place = Place.query.filter_by(id=place_id).first()
    db.session.delete(place)
    db.session.commit()
    flash('Цех удален!', 'danger')
    return redirect(url_for('main.index'))


@bp.route('/edit_line/<int:place_id>/<line_id>', methods=['POST', 'GET'])
@login_required
def edit_line(place_id, line_id):
    edit_line_form = AddLine()
    line = Line.query.filter_by(id=line_id, place_id=place_id).first_or_404()
    employees = [(c.id, c.last_name) for c in Employee.query.all()]
    edit_line_form.employee.choices = employees

    if edit_line_form.validate_on_submit():
        line.name = edit_line_form.name.data
        line.employee_id = edit_line_form.employee.data
        db.session.commit()
        flash('Данные успешно изменены', 'success')

        return redirect(url_for('main.place', place_id=place_id))

    elif request.method == 'GET':
        edit_line_form.name.data = line.name
        edit_line_form.employee.data = line.employee_id

    return render_template('edit_line.html', title='Редактирование линии', edit_line_form=edit_line_form,
                           place_id=place_id, line_id=line_id, index=True)


@bp.route('/delete_line/<int:place_id>/<int:line_id>', methods=['POST', 'GET'])
@login_required
def delete_line(place_id, line_id):
    line = Line.query.filter_by(id=line_id, place_id=place_id).first()
    db.session.delete(line)
    db.session.commit()
    flash('Линия удалена!', 'danger')
    return redirect(url_for('main.place', place_id=place_id))


@bp.route('/place<int:place_id>/', methods=['GET', 'POST'])
@login_required
def place(place_id):
    add_line = AddLine()
    lines = Line.query.all()
    place = Place.query.all()
    employees = [(c.id, c.last_name) for c in Employee.query.all()]
    add_line.employee.choices = employees

    if add_line.validate_on_submit():
        query = Line(name=add_line.name.data, place_id=place_id, user_id=current_user.id,
                     employee_id=add_line.employee.data)
        db.session.add(query)
        db.session.commit()
        flash('Ваши изменения успешно сохранены')
        return redirect(url_for('main.place', place_id=place_id))
    elif request.method == 'GET':
        lines = Line.query.filter(Line.place_id == place_id).all()
    return render_template('place.html', title='Главная', index=False, place_id=place_id, lines=lines, place=place,
                           add_line=add_line)


@bp.route('/place<int:place_id>/line<int:line_id>/', methods=['GET', 'POST'])
@login_required
def line(place_id, line_id):
    add_equipment = AddEquipment(request.form)
    # Автозаполнение SelectField из таблицы Machine в базе
    categories = [(c.id, c.name) for c in Machine.query.all()]
    add_equipment.equipment_type.choices = categories
    lines = Line.query.all()
    equipment = Equipment.query.filter(Equipment.line_id == line_id).all()
    place = Place.query.all()
    if add_equipment.validate_on_submit():
        query = Equipment(name=add_equipment.name.data, place_id=place_id, line_id=line_id,
                          description=add_equipment.description.data, rnkb=add_equipment.rnkb.data,
                          user_id=current_user.id, tech_data=add_equipment.tech_data.data,
                          equipment_type_id=add_equipment.equipment_type.data)
        db.session.add(query)
        db.session.commit()
        flash('Ваши изменения успешно сохранены')
        return redirect(url_for('main.line', place_id=place_id, line_id=line_id))
    elif request.method == 'GET':
        equipment = Equipment.query.filter(Equipment.line_id == line_id).order_by(Equipment.name).all()
    return render_template('line.html', title='Оборудование на линии', line=True, place_id=place_id, line_id=line_id,
                           add_equipment=add_equipment, equipment=equipment, lines=lines, place=place)


@bp.route('/follow/<username>')
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash("Пользователь %s не существует." % username)
        return redirect(url_for('main.index'))
    if user == current_user:
        flash('Вы не можете подписаться на свои обновления!')
        return redirect(url_for('main.user', username=username))
    current_user.follow(user)
    db.session.commit()
    flash('Вы подписались на пользователя %s !' % username)
    return redirect(url_for('main.user', username=username))


@bp.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Пользователь %s не существует.', username)
        return redirect(url_for('main.index'))
    if user == current_user:
        flash('Вы не можете отписаться от своих обновлений!')
        return redirect(url_for('main.user', username=username))
    current_user.unfollow(user)
    db.session.commit()
    flash('Вы отписались от пользователя %s.' % username)
    return redirect(url_for('main.user', username=username))


@bp.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    return render_template('user.html', user=user, profile=True, title=username)


@bp.route('/create_a_task')
@login_required
def create_a_task():
    task_form = CreateTask()
    places = [(c.id, c.name) for c in Place.query.filter_by(user_id=current_user.id).all()]
    task_form.place.choices = places

    # lines = [(c.id, c.name) for c in Line.query.filter_by(user_id=current_user.id).all()]
    # task_form.line.choices = lines

    return render_template('create_a_task.html', create_task=True, task_form=task_form)


@bp.route('/line/<place>')
@login_required
def pick_line(place):
    lines = Line.query.filter_by(place_id=place).all()

    lineArray = []
    for line in lines:
        lineObj = {}
        lineObj['id'] = line.id
        lineObj['name'] = line.name
        lineArray.append(lineObj)

    return jsonify({'lines': lineArray})


@bp.route('/equipment/<line>')
@login_required
def pick_equipment(line):
    equipment = Equipment.query.filter_by(line_id=line).all()

    equipmentArray = []
    for equip in equipment:
        equipmentObj = {}
        equipmentObj['id'] = equip.id
        equipmentObj['name'] = equip.name
        equipmentArray.append(equipmentObj)

    return jsonify({'equipment' : equipmentArray})
