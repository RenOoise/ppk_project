from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SubmitField, SelectField
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms.validators import DataRequired
from app import images
from app.models import Employee


class EditEquipment(FlaskForm):
    name = StringField('Наименование оборудования', validators=[DataRequired()])
    # place = SelectField(validators=[DataRequired()], choices=SelectLine(), label='Линия установки оборудования')
    rnkb = StringField('Номер RNKB')
    description = TextAreaField('Краткое описание оборудования')
    tech_data = TextAreaField('Технические данные')
    equipment_type = SelectField('Тип оборудования', choices=[], coerce=int)
    image = FileField('Фото', validators=[FileAllowed(images, 'Images only!')])
    submit = SubmitField('Изменить')

