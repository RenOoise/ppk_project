from flask import render_template, flash, redirect, url_for, request, g
from flask_login import current_user, login_required
from app import db
from app.equipment.forms import EditEquipment
from app.models import User, Place, Line, Equipment, Machine, Employee
from app.main import bp
from app import images


@bp.route('/place<int:place_id>/line<int:line_id>/id<int:equipment_id>')
@login_required
def equipment(place_id, line_id, equipment_id):
    equipment = Equipment.query.filter_by(id=equipment_id).first_or_404()
    lines = Line.query.all()
    place = Place.query.all()

    return render_template('equipment/equipment.html', title=equipment.name, equipment=equipment, equipment_title=True,
                           place_id=place_id, line_id=line_id, equipment_id=equipment_id, lines=lines, place=place)


@bp.route('/place<int:place_id>/line<int:line_id>/id<int:equipment_id>/edit', methods=['POST', 'GET'])
@login_required
def edit_equipment(place_id, line_id, equipment_id):
    edit_form = EditEquipment(request.form)
    categories = [(c.id, c.name) for c in Machine.query.all()]
    edit_form.equipment_type.choices = categories

    if edit_form.validate_on_submit():
        query = Equipment.query.filter(Equipment.id == equipment_id and Equipment.line_id == line_id).first_or_404()
        query.name = edit_form.name.data
        query.rnkb = edit_form.rnkb.data
        query.description = edit_form.description.data
        query.tech_data = edit_form.tech_data.data
        query.equipment_type_id = edit_form.equipment_type.data

        # Здесь делаем проверку, прикреплена ли фотография, если да, то заливаем ее и кидаем в базу урлы,
        # Ежели нет, то оставляем старую и вообще не обрабатываем этот блок
        # TODO: Исправить косяк с вевозможностью загружать файлы с кириллицей в названии
        try:
            filename = images.save(request.files['image'])
            url = images.url(filename)
            query.image_filename = filename
            query.image_url = url
        except Exception as e:
            pass
            print(e)
        finally:
            db.session.commit()
            flash('Ваши изменения сохранены')

        return redirect(url_for('main.equipment', place_id=place_id, line_id=line_id, equipment_id=equipment_id))
    elif request.method == 'GET':
        query = Equipment.query.filter(Equipment.id == equipment_id and Equipment.line_id == line_id).first_or_404()
        edit_form.name.data = query.name
        edit_form.rnkb.data = query.rnkb
        edit_form.description.data = query.description
        edit_form.tech_data.data = query.tech_data
        edit_form.equipment_type.data = query.equipment_type_id
    return render_template('equipment/edit_equipment.html', title='Редактирование профиля оборудования',
                           edit_form=edit_form, place_id=place_id, line_id=line_id, equipment_id=equipment_id)
